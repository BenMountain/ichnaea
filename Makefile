CXX ?= g++
# General linker for pthread and libdarknet.so
#This assumes libdarknet.so is copied to /usr/local/lib.
LDFLAGS += -pthread -ldarknet

CXXFLAGS += -c -std=c++11 -Wall $(shell pkg-config --cflags opencv)

# opencv
LDFLAGS += $(shell pkg-config --libs opencv)

# CUDA / GPU
LDFLAGS += -L/usr/local/cuda/lib64

PROG=ichnaea
OBJS=$(PROG).o

all: $(PROG)

$(PROG): $(OBJS)
	$(CXX) $< -o $@ $(LDFLAGS)

%.o: %.cpp
	$(CXX) $< -o $@ $(CXXFLAGS)

clean:
	rm -f $(OBJS) $(PROG)
