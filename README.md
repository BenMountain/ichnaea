# Ichnaea is a tracking and object counting program.
In Greek mythology, Ichnaea was the goddess of tracking.
Uses YOLO object tracking and detection as the basis for frame-to-frame object detection and classification through a library call.
Object counting and tracking within a video builds on this in addition to optical flow and Kalman filters. Original code forked from yolo_console_dll.cpp by [AlexeyAB](https://github.com/AlexeyAB/darknet/blob/master/src/yolo_console_dll.cpp).

## Compile Ichnaea (Linux):
1. To use the program compile darknet from the AlexeyAB branch of [Darknet](https://github.com/AlexeyAB/darknet). Follow instructions for that repository to compile and build using the following options.
2. (Darknet) Build and compile Darknet for CUDA and CUDNN for highest performance on NVidia GPUs.
3. (Darknet) Build the libdarknet.so library by enabling LIBSO=1 in the darknet Makefile
4. (Darknet) [OPTIONAL] Build with OpenCV.
5. (Linux Terminal)Install the libdarknet.so file to the standard system directory.

    `sudo cp libdarknet.so /usr/local/lib`
    
    `sudo ldconfig`
    
6. Compile Ichnaea by running `make` in terminal from program directory to compile.

## Run Ichnaea
Ichnaea is a darknet API. As such, it relies on model files and weight files from darknet to run. If a new weight file or cfg file is needed then it may need to be trained or tested using darknet first.

## Example execution codes:
In the example below the Ichnaea program is pulling coco.names and yolov3.cfg files from the darknet folder. Also the weights file is mainted up one directory from both darknet and Ichnaea for cross-usage.

`./ichnaea ../darknet/data/coco.names ../darknet/cfg/yolov3.cfg ../yolov3.weights video.mp4`

Or use the command below to run detections on a live stream (http, https, rtsp, or rtmp)

`./ichnaea ../darknet/data/coco.names ../darknet/cfg/yolov3.cfg ../yolov3.weights rtsp://192.168.1.138:8080/video/h264`
